package com.demo.postapp.usecase.comment

import com.demo.postapp.data.repository.CommentRepository
import com.demo.postapp.domain.CommentDomain
import kotlinx.coroutines.flow.Flow

class GetLocalCommentsUseCase(
    private val commentRepository: CommentRepository
) {

    operator fun invoke(idPost: Int): Flow<List<CommentDomain>> =
        commentRepository.getLocalComments(idPost)

}