package com.demo.postapp.usecase.post

import com.demo.postapp.data.repository.PostRepository
import com.demo.postapp.domain.PostDomain
import kotlinx.coroutines.flow.Flow

class GetLocalPostUseCase(
    private val postRepository: PostRepository
) {

    operator fun invoke(idPost: Int): Flow<PostDomain> = postRepository.getLocalPost(idPost)

}