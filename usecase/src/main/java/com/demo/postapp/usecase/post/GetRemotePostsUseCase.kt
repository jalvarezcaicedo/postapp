package com.demo.postapp.usecase.post

import arrow.core.Either
import com.demo.postapp.data.repository.PostRepository
import com.demo.postapp.domain.Error
import com.demo.postapp.domain.PostDomain
import kotlinx.coroutines.flow.Flow

class GetRemotePostsUseCase(
    private val postRepository: PostRepository
) {

    suspend operator fun invoke(): Either<Error,List<PostDomain>> = postRepository.getPostData()

}