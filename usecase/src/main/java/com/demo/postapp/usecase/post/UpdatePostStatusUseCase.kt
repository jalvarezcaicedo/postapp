package com.demo.postapp.usecase.post

import com.demo.postapp.data.repository.PostRepository
import com.demo.postapp.domain.PostDomain


class UpdatePostStatusUseCase(
    private val postRepository: PostRepository
) {

    suspend operator fun invoke(post: PostDomain, favorite: Int) =
        postRepository.updatePostStatus(post, favorite)

}