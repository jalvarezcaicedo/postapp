package com.demo.postapp.usecase.post

import com.demo.postapp.data.repository.PostRepository
import com.demo.postapp.domain.PostDomain


class SavePostsUseCase(
    private val postRepository: PostRepository
) {

    suspend operator fun invoke(posts: List<PostDomain>) = postRepository.savePostData(posts)

}