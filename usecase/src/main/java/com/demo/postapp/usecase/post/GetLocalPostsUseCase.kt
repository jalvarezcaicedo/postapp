package com.demo.postapp.usecase.post

import com.demo.postapp.data.repository.PostRepository
import com.demo.postapp.domain.PostDomain
import kotlinx.coroutines.flow.Flow

class GetLocalPostsUseCase(
    private val postRepository: PostRepository
) {

    operator fun invoke(): Flow<List<PostDomain>> = postRepository.getLocalPosts()

}