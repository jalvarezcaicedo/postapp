package com.demo.postapp.usecase.comment

import arrow.core.Either
import com.demo.postapp.data.repository.CommentRepository
import com.demo.postapp.domain.CommentDomain
import com.demo.postapp.domain.Error

class GetRemoteCommentsUseCase(
    private val commentRepository: CommentRepository
) {

    suspend operator fun invoke(idPost: Int): Either<Error, List<CommentDomain>> =
        commentRepository.getCommentData(idPost)

}