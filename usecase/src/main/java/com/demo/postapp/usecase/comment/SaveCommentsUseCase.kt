package com.demo.postapp.usecase.comment

import com.demo.postapp.data.repository.CommentRepository
import com.demo.postapp.domain.CommentDomain


class SaveCommentsUseCase(
    private val commentRepository: CommentRepository
) {

    suspend operator fun invoke(comments: List<CommentDomain>) =
        commentRepository.saveCommentData(comments)

}