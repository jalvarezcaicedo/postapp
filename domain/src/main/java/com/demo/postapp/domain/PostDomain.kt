package com.demo.postapp.domain

data class PostDomain(
    var id: Int,
    var userId: Int,
    var title: String,
    var body: String,
    var favorite: Int = 0
)