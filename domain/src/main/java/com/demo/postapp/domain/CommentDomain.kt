package com.demo.postapp.domain

data class CommentDomain(
    var id: Int,
    var postId: Int,
    var name: String,
    var email: String,
    var body: String
)