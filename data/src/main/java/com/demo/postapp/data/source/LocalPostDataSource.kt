package com.demo.postapp.data.source

import com.demo.postapp.domain.PostDomain
import kotlinx.coroutines.flow.Flow

interface LocalPostDataSource {

    fun getLocalPost(idPost: Int): Flow<PostDomain>
    fun getLocalPostData(): Flow<List<PostDomain>>
    fun getLocalPostDataFiltered(query: String): Flow<List<PostDomain>>

    suspend fun savePosts(posts: List<PostDomain>)
    suspend fun updatePostState(idPost: String, favorite: Int)
}