package com.demo.postapp.data.source

import arrow.core.Either
import com.demo.postapp.domain.Error
import com.demo.postapp.domain.PostDomain

interface RemotePostDataSource {

    suspend fun getPosts(): Either<Error, List<PostDomain>>

}