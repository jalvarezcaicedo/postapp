package com.demo.postapp.data.source

import arrow.core.Either
import com.demo.postapp.domain.CommentDomain
import com.demo.postapp.domain.Error

interface RemoteCommentDataSource {

    suspend fun getComments(idPost: Int): Either<Error, List<CommentDomain>>

}