package com.demo.postapp.data.repository

import arrow.core.Either
import com.demo.postapp.data.source.LocalCommentDataSource
import com.demo.postapp.data.source.RemoteCommentDataSource
import com.demo.postapp.domain.CommentDomain
import com.demo.postapp.domain.Error
import kotlinx.coroutines.flow.Flow

class CommentRepository(
    private val localCommentDataSource: LocalCommentDataSource,
    private val remoteCommentDataSource: RemoteCommentDataSource
) {

    suspend fun saveCommentData(commentData: List<CommentDomain>) {
        localCommentDataSource.saveComment(commentData)
    }

    suspend fun getCommentData(idPost: Int): Either<Error, List<CommentDomain>> =
        remoteCommentDataSource.getComments(idPost)

    fun getLocalComments(idPost: Int): Flow<List<CommentDomain>> =
        localCommentDataSource.getLocalCommentData(idPost)

}