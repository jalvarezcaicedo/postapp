package com.demo.postapp.data.repository

import arrow.core.Either
import com.demo.postapp.data.source.LocalPostDataSource
import com.demo.postapp.data.source.RemotePostDataSource
import com.demo.postapp.domain.Error
import com.demo.postapp.domain.PostDomain
import kotlinx.coroutines.flow.Flow

class PostRepository(
    private val localPostDataSource: LocalPostDataSource,
    private val remotePostDataSource: RemotePostDataSource
) {

    suspend fun savePostData(postData: List<PostDomain>) {
        localPostDataSource.savePosts(postData)
    }

    suspend fun updatePostStatus(post: PostDomain, favorite: Int) {
        localPostDataSource.updatePostState(post.id.toString(), favorite)
    }

    suspend fun getPostData(): Either<Error, List<PostDomain>> = remotePostDataSource.getPosts()

    fun getLocalPostFiltered(query: String): Flow<List<PostDomain>> =
        localPostDataSource.getLocalPostDataFiltered(query)

    fun getLocalPosts(): Flow<List<PostDomain>> = localPostDataSource.getLocalPostData()

    fun getLocalPost(idPost: Int): Flow<PostDomain> = localPostDataSource.getLocalPost(idPost)

}