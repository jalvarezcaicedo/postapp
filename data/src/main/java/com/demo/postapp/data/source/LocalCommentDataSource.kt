package com.demo.postapp.data.source

import com.demo.postapp.domain.CommentDomain
import kotlinx.coroutines.flow.Flow

interface LocalCommentDataSource {

    fun getLocalCommentData(idPost: Int): Flow<List<CommentDomain>>

    suspend fun saveComment(comments: List<CommentDomain>)
}