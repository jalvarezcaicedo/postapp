package com.demo.postapp.business.server.service

import com.demo.postapp.domain.CommentDomain
import retrofit2.http.GET
import retrofit2.http.Path

interface CommentsApi {
    @GET("/posts/{postId}/comments")
    suspend fun getComments(
        @Path("postId") postId: Int
    ): List<CommentDomain>
}