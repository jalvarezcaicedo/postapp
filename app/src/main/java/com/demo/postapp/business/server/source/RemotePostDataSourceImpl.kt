package com.demo.postapp.business.server.source

import arrow.core.Either
import com.demo.postapp.business.server.service.PostApi
import com.demo.postapp.business.util.tryCall
import com.demo.postapp.data.source.RemotePostDataSource
import com.demo.postapp.domain.Error
import com.demo.postapp.domain.PostDomain

class RemotePostDataSourceImpl(
    private val postApi: PostApi
) : RemotePostDataSource {

    override suspend fun getPosts(): Either<Error, List<PostDomain>> =
        tryCall {
            postApi.getPosts().map {
                it
            }
        }

}