package com.demo.postapp.business.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Post")
data class PostEntity(
    @PrimaryKey var id: Int,
    var userId: Int,
    var title: String,
    var body: String,
    var favorite: Int
)