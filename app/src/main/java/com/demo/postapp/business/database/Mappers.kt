package com.demo.postapp.business.database

import com.demo.postapp.business.database.entity.CommentEntity
import com.demo.postapp.business.database.entity.PostEntity
import com.demo.postapp.domain.CommentDomain
import com.demo.postapp.domain.PostDomain

fun PostDomain.toPostEntity() = PostEntity(
    id, userId, title, body, 0
)

fun PostEntity.toPostDomain() = PostDomain(
    id, userId, title, body, favorite
)

fun List<PostEntity>.toPostDomainList(): List<PostDomain> = map {
    it.toPostDomain()
}

fun CommentDomain.toCommentEntity() = CommentEntity(
    id, postId, name, email, body
)

fun CommentEntity.toCommentDomain() = CommentDomain(
    id, postId, name, email, body
)

fun List<CommentEntity>.toCommentDomainList(): List<CommentDomain> = map {
    it.toCommentDomain()
}