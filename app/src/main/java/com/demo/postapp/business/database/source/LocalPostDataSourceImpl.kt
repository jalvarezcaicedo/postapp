package com.demo.postapp.business.database.source

import com.demo.postapp.business.database.PostsDB
import com.demo.postapp.business.database.toPostDomain
import com.demo.postapp.business.database.toPostDomainList
import com.demo.postapp.business.database.toPostEntity
import com.demo.postapp.data.source.LocalPostDataSource
import com.demo.postapp.domain.PostDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class LocalPostDataSourceImpl(
    private val db: PostsDB
) : LocalPostDataSource {

    private val postDao by lazy { db.postDao() }

    override fun getLocalPostData(): Flow<List<PostDomain>> = postDao.findPosts().map {
        it.toPostDomainList()
    }

    override fun getLocalPost(idPost: Int): Flow<PostDomain> = postDao.findPost(idPost).map {
        it.toPostDomain()
    }

    override fun getLocalPostDataFiltered(query: String): Flow<List<PostDomain>> =
        postDao.findPostsFiltered(query).map {
            it.toPostDomainList()
        }

    override suspend fun savePosts(posts: List<PostDomain>) {
        postDao.insertAll(posts.map { it.toPostEntity() })
    }

    override suspend fun updatePostState(idPost: String, favorite: Int) {
        postDao.updatePostFavorite(idPost, favorite)
    }
}