package com.demo.postapp.business.server.source

import arrow.core.Either
import com.demo.postapp.business.server.service.CommentsApi
import com.demo.postapp.business.util.tryCall
import com.demo.postapp.data.source.RemoteCommentDataSource
import com.demo.postapp.domain.CommentDomain
import com.demo.postapp.domain.Error

class RemoteCommentDataSourceImpl(
    private val commentsApi: CommentsApi
) : RemoteCommentDataSource {

    override suspend fun getComments(idPost: Int): Either<Error, List<CommentDomain>> =
        tryCall {
            commentsApi.getComments(idPost).map {
                it
            }
        }
    
}