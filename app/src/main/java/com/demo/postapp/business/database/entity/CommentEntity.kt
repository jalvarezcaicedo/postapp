package com.demo.postapp.business.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Comment")
data class CommentEntity(
    @PrimaryKey var id: Int,
    var postId: Int,
    var name: String,
    var email: String,
    var body: String
)