package com.demo.postapp.business.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.demo.postapp.business.database.dao.CommentDao
import com.demo.postapp.business.database.dao.PostDao
import com.demo.postapp.business.database.entity.CommentEntity
import com.demo.postapp.business.database.entity.PostEntity

@Database(
    entities = [
        PostEntity::class,
        CommentEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class PostsDB : RoomDatabase() {

    abstract fun postDao(): PostDao

    abstract fun commentDao(): CommentDao

    companion object {
        @Synchronized
        fun getDatabase(context: Context): PostsDB = Room.databaseBuilder(
            context.applicationContext, PostsDB::class.java, "posts_db"
        ).build()
    }

}