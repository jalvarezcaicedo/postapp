package com.demo.postapp.business.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.demo.postapp.business.database.entity.PostEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(postData: List<PostEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPost(post: PostEntity)

    @Query("SELECT * FROM Post ORDER BY favorite DESC")
    fun findPosts(): Flow<List<PostEntity>>

    @Query("SELECT * FROM Post WHERE id = :idPost")
    fun findPost(idPost: Int): Flow<PostEntity>

    @Query("SELECT * FROM Post WHERE title LIKE '%'||:query||'%' ORDER BY favorite DESC")
    fun findPostsFiltered(query: String): Flow<List<PostEntity>>

    @Query("UPDATE Post SET favorite =:favorite WHERE id = :idPost")
    suspend fun updatePostFavorite(idPost: String, favorite: Int)

}