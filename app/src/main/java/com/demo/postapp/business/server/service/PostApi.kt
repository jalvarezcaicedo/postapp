package com.demo.postapp.business.server.service

import com.demo.postapp.domain.PostDomain
import retrofit2.http.GET

interface PostApi {
    @GET("/posts")
    suspend fun getPosts(): List<PostDomain>
}