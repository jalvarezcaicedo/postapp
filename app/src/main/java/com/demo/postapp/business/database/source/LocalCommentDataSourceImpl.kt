package com.demo.postapp.business.database.source

import com.demo.postapp.business.database.PostsDB
import com.demo.postapp.business.database.toCommentDomainList
import com.demo.postapp.business.database.toCommentEntity
import com.demo.postapp.data.source.LocalCommentDataSource
import com.demo.postapp.domain.CommentDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class LocalCommentDataSourceImpl(
    private val db: PostsDB
) : LocalCommentDataSource {

    private val commentDao by lazy { db.commentDao() }

    override fun getLocalCommentData(idPost: Int): Flow<List<CommentDomain>> =
        commentDao.findComments(idPost).map {
            it.toCommentDomainList()
        }

    override suspend fun saveComment(comments: List<CommentDomain>) {
        commentDao.insertAll(comments.map { it.toCommentEntity() })
    }

}