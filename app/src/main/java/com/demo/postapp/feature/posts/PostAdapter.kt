package com.demo.postapp.feature.posts

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.demo.postapp.R
import com.demo.postapp.databinding.ItemPostBinding
import com.demo.postapp.domain.PostDomain

class PostAdapter(
    private val listener: PostItemListener
) : RecyclerView.Adapter<PostAdapter.PostViewHolder>() {

    private val differCallback = object : DiffUtil.ItemCallback<PostDomain>() {
        override fun areItemsTheSame(oldItem: PostDomain, newItem: PostDomain): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: PostDomain, newItem: PostDomain): Boolean {
            return oldItem == newItem
        }
    }

    private val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_post, parent, false)
        return PostViewHolder(view)
    }

    override fun getItemCount(): Int = differ.currentList.size

    fun submitPostData(posts: List<PostDomain>) {
        differ.submitList(posts)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    inner class PostViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        private val binding = ItemPostBinding.bind(view)

        fun bind(item: PostDomain) {
            binding.clPostItem.setOnClickListener {
                listener.onSelectPostListener(item)
            }

            binding.tvId.text = item.id.toString()
            binding.tvTitle.text = item.title
            binding.cbFavorite.isChecked = item.favorite != 0

            binding.cbFavorite.setOnCheckedChangeListener { _, isChecked ->
                binding.cbFavorite.isChecked = isChecked
                if (isChecked) listener.onFavoriteSelected(item, 1)
                else listener.onFavoriteSelected(item, 0)
            }

        }
    }
}