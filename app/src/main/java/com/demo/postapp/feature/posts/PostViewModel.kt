package com.demo.postapp.feature.posts

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.demo.postapp.domain.Error
import com.demo.postapp.domain.PostDomain
import com.demo.postapp.usecase.post.GetLocalPostsUseCase
import com.demo.postapp.usecase.post.GetRemotePostsUseCase
import com.demo.postapp.usecase.post.SavePostsUseCase
import com.demo.postapp.usecase.post.UpdatePostStatusUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PostViewModel @Inject constructor(
    private val savePostsUseCase: SavePostsUseCase,
    private val updatePostStatusUseCase: UpdatePostStatusUseCase,
    private val getLocalPostsUseCase: GetLocalPostsUseCase,
    private val getRemotePostsUseCase: GetRemotePostsUseCase
) : ViewModel() {

    private val _state = MutableStateFlow(UIState())
    val state: StateFlow<UIState> = _state.asStateFlow()

    fun savePostData(posts: List<PostDomain>) = viewModelScope.launch {
        savePostsUseCase.invoke(posts)
    }

    fun updatePostStatus(post: PostDomain, favorite: Int) = viewModelScope.launch {
        updatePostStatusUseCase.invoke(post, favorite)
    }

    fun getPostDataServer() = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getRemotePostsUseCase().fold(ifLeft = { error ->
            _state.update { it.copy(error = error, loading = false) }
        }, ifRight = { response ->
            _state.update { it.copy(remotePosts = response, loading = false) }
        })
    }

    fun getLocalPosts() = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getLocalPostsUseCase().catch { }.collect { posts ->
            _state.update { UIState(localPosts = posts) }
        }
    }

    data class UIState(
        val error: Error? = null,
        val loading: Boolean = false,
        val remotePosts: List<PostDomain>? = null,
        val localPosts: List<PostDomain>? = null
    )

}