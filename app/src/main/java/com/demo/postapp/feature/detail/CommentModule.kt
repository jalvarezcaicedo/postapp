package com.demo.postapp.feature.detail

import com.demo.postapp.data.repository.CommentRepository
import com.demo.postapp.usecase.comment.GetLocalCommentsUseCase
import com.demo.postapp.usecase.comment.GetRemoteCommentsUseCase
import com.demo.postapp.usecase.comment.SaveCommentsUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
class CommentModule {

    @Provides
    @ViewModelScoped
    fun saveCommentsUseCase(
        commentRepository: CommentRepository
    ) = SaveCommentsUseCase(commentRepository)

    @Provides
    @ViewModelScoped
    fun getLocalCommentsUseCase(
        commentRepository: CommentRepository
    ) = GetLocalCommentsUseCase(commentRepository)

    @Provides
    @ViewModelScoped
    fun getRemoteCommentsUseCase(
        commentRepository: CommentRepository
    ) = GetRemoteCommentsUseCase(commentRepository)

}