package com.demo.postapp.feature.posts

import com.demo.postapp.domain.PostDomain

interface PostItemListener {

    fun onSelectPostListener(postDomain: PostDomain)

    fun onFavoriteSelected(postDomain: PostDomain, favorite: Int)
}