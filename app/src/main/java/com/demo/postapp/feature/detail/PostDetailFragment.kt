package com.demo.postapp.feature.detail

import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.postapp.databinding.FragmentPostDetailBinding
import com.demo.postapp.domain.CommentDomain
import com.demo.postapp.extension.launchAndCollect
import com.empowerlabs.myrecipes.feature.common.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map

@AndroidEntryPoint
class PostDetailFragment : BaseFragment<FragmentPostDetailBinding>() {

    private val commentData = mutableListOf<CommentDomain>()

    override fun initView() {

        binding.rvComments.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(this@PostDetailFragment.requireContext(), 3)
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.HORIZONTAL))
        }

    }

    override fun initListeners() = Unit

    override fun initObservables() {

    }

    override fun getParametersFragment() = Unit

    override fun getViewBinding(): FragmentPostDetailBinding =
        FragmentPostDetailBinding.inflate(layoutInflater)

    private fun <T, U> Flow<T>.diff(mapFlow: (T) -> U, body: (U) -> Unit) {
        viewLifecycleOwner.launchAndCollect(
            flow = map(mapFlow).distinctUntilChanged(), body = body
        )
    }

}
