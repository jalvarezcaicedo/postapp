package com.demo.postapp.feature.posts

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.demo.postapp.R
import com.demo.postapp.databinding.FragmentPostsBinding
import com.demo.postapp.domain.PostDomain
import com.demo.postapp.extension.launchAndCollect
import com.demo.postapp.feature.util.SwipeToDeleteCallback
import com.empowerlabs.myrecipes.feature.common.BaseFragment
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import timber.log.Timber

@AndroidEntryPoint
class PostsFragment : BaseFragment<FragmentPostsBinding>(), PostItemListener {

    private val postAdapter: PostAdapter = PostAdapter(this)
    private val postViewModel: PostViewModel by viewModels()
    private val postData = mutableListOf<PostDomain>()

    override fun initView() {
        setMenu()

        binding.rvPosts.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@PostsFragment.requireContext())
            adapter = postAdapter
        }

        val swipeToDeleteCallback = object : SwipeToDeleteCallback() {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.bindingAdapterPosition
                //TODO: call use case removing from viewmodel
                postData.removeAt(position)
                binding.rvPosts.adapter?.notifyItemChanged(position)
            }
        }

        val itemTouchHelper = ItemTouchHelper(swipeToDeleteCallback)

        itemTouchHelper.attachToRecyclerView(binding.rvPosts)
    }

    private fun setMenu() {
        val menuHost: MenuHost = requireActivity()
        menuHost.addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.post_menu, menu)
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.action_load -> {
                        postViewModel.getPostDataServer()
                        true
                    }
                    R.id.action_remove -> {
                        postViewModel.getLocalPosts()
                        true
                    }
                    else -> false
                }
            }

        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }

    override fun initListeners() = Unit

    override fun initObservables() {
        with(postViewModel.state) {
            diff({ it.loading }) { handleVisibility(it, listOf()) }
            diff({ it.remotePosts }) {
                it?.let { postViewModel.savePostData(it) }
            }
            diff({ it.localPosts }) {
                it?.let {
                    postData.clear()
                    postData.addAll(it)
                    handleUpdatePost(it)
                }
            }
            diff({ it.error }) {
                it?.let {
                    Snackbar.make(binding.clPosts, it.toString(), Snackbar.LENGTH_SHORT).show()
                }
            }
        }

        postViewModel.getPostDataServer()
        postViewModel.getLocalPosts()
    }

    override fun getParametersFragment() {}

    override fun getViewBinding(): FragmentPostsBinding =
        FragmentPostsBinding.inflate(layoutInflater)

    private fun handleVisibility(loading: Boolean, data: List<PostDomain>) {
        Timber.d(loading.toString().plus(" $data"))
    }

    override fun onSelectPostListener(postDomain: PostDomain) {
        val navAction = PostsFragmentDirections.actionPostToDetail(
            postDomain.title,
            postDomain.body,
            postDomain.id
        )
        findNavController().navigate(navAction)
    }

    override fun onFavoriteSelected(postDomain: PostDomain, favorite: Int) {
        postViewModel.updatePostStatus(postDomain, favorite)
    }

    private fun handleUpdatePost(posts: List<PostDomain>) {
        handleVisibility(false, posts)
        postAdapter.submitPostData(posts)
        binding.rvPosts.smoothScrollToPosition(0)
    }

    private fun <T, U> Flow<T>.diff(mapFlow: (T) -> U, body: (U) -> Unit) {
        viewLifecycleOwner.launchAndCollect(
            flow = map(mapFlow).distinctUntilChanged(), body = body
        )
    }

}
