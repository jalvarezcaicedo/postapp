package com.demo.postapp.feature.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.demo.postapp.domain.CommentDomain
import com.demo.postapp.domain.Error
import com.demo.postapp.usecase.comment.GetLocalCommentsUseCase
import com.demo.postapp.usecase.comment.GetRemoteCommentsUseCase
import com.demo.postapp.usecase.comment.SaveCommentsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PostDetailViewModel @Inject constructor(
    private val saveCommentsUseCase: SaveCommentsUseCase,
    private val getLocalCommentsUseCase: GetLocalCommentsUseCase,
    private val getRemoteCommentsUseCase: GetRemoteCommentsUseCase
) : ViewModel() {

    private val _state = MutableStateFlow(UIState())
    val state: StateFlow<UIState> = _state.asStateFlow()

    fun saveCommentsData(comments: List<CommentDomain>) = viewModelScope.launch {
        saveCommentsUseCase.invoke(comments)
    }

    fun getCommentDataServer(idPost: Int) = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getRemoteCommentsUseCase(idPost).fold(ifLeft = { error ->
            _state.update { it.copy(error = error, loading = false) }
        }, ifRight = { response ->
            _state.update { it.copy(remoteComments = response, loading = false) }
        })
    }

    fun getLocalComments(idPost: Int) = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getLocalCommentsUseCase(idPost).catch { }.collect { comments ->
            _state.update { UIState(localComments = comments) }
        }
    }

    data class UIState(
        val error: Error? = null,
        val loading: Boolean = false,
        val remoteComments: List<CommentDomain>? = null,
        val localComments: List<CommentDomain>? = null
    )

}