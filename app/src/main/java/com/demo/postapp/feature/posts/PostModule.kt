package com.demo.postapp.feature.posts

import com.demo.postapp.data.repository.PostRepository
import com.demo.postapp.usecase.post.GetLocalPostsUseCase
import com.demo.postapp.usecase.post.GetRemotePostsUseCase
import com.demo.postapp.usecase.post.SavePostsUseCase
import com.demo.postapp.usecase.post.UpdatePostStatusUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
class PostModule {

    @Provides
    @ViewModelScoped
    fun savePostUseCase(
        postRepository: PostRepository
    ) = SavePostsUseCase(postRepository)

    @Provides
    @ViewModelScoped
    fun updatePostStatusUseCase(
        postRepository: PostRepository
    ) = UpdatePostStatusUseCase(postRepository)

    @Provides
    @ViewModelScoped
    fun getLocalPostsUseCase(
        postRepository: PostRepository
    ) = GetLocalPostsUseCase(postRepository)

    @Provides
    @ViewModelScoped
    fun getRemotePostsUseCase(
        postRepository: PostRepository
    ) = GetRemotePostsUseCase(postRepository)

}