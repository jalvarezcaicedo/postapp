package com.demo.postapp.module

import android.app.Application
import com.demo.postapp.business.database.PostsDB
import com.demo.postapp.business.database.source.LocalCommentDataSourceImpl
import com.demo.postapp.business.database.source.LocalPostDataSourceImpl
import com.demo.postapp.business.server.service.CommentsApi
import com.demo.postapp.business.server.service.PostApi
import com.demo.postapp.business.server.source.RemoteCommentDataSourceImpl
import com.demo.postapp.business.server.source.RemotePostDataSourceImpl
import com.demo.postapp.data.repository.CommentRepository
import com.demo.postapp.data.repository.PostRepository
import com.demo.postapp.data.source.LocalCommentDataSource
import com.demo.postapp.data.source.LocalPostDataSource
import com.demo.postapp.data.source.RemoteCommentDataSource
import com.demo.postapp.data.source.RemotePostDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class Data {

    @Provides
    @Singleton
    fun databaseProvider(app: Application): PostsDB = PostsDB.getDatabase(app)

    @Provides
    fun postRepository(
        localPostDataSource: LocalPostDataSource, remotePostDataSource: RemotePostDataSource
    ) = PostRepository(localPostDataSource, remotePostDataSource)

    @Provides
    fun commentRepository(
        localCommentDataSource: LocalCommentDataSource,
        remoteCommentDataSource: RemoteCommentDataSource
    ) = CommentRepository(localCommentDataSource, remoteCommentDataSource)

    @Provides
    fun localPostDataSourceProvider(
        db: PostsDB
    ): LocalPostDataSource = LocalPostDataSourceImpl(db)

    @Provides
    fun remotePostDataSourceProvider(
        postApi: PostApi
    ): RemotePostDataSource = RemotePostDataSourceImpl(postApi)

    @Provides
    fun localCommentDataSourceProvider(
        db: PostsDB
    ): LocalCommentDataSource = LocalCommentDataSourceImpl(db)

    @Provides
    fun remoteCommentDataSourceProvider(
        commentsApi: CommentsApi
    ): RemoteCommentDataSource = RemoteCommentDataSourceImpl(commentsApi)

}