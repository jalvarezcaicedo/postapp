package com.demo.postapp.module

import com.demo.postapp.business.server.service.CommentsApi
import com.demo.postapp.business.server.service.PostApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
class Api {

    @Provides
    fun providePostApi(retrofit: Retrofit): PostApi = retrofit.create(PostApi::class.java)

    @Provides
    fun provideCommentApi(retrofit: Retrofit): CommentsApi =
        retrofit.create(CommentsApi::class.java)

}